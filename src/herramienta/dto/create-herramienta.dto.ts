import { ApiProperty } from "@nestjs/swagger";
export class CreateHerramientaDto {
  @ApiProperty({ type: "string", example: "1" })
  id: string;
  @ApiProperty({ type: "string", example: "Pacho Sa" })
  nombre: string;
  @ApiProperty({ type: "number", example: "10" })
  peso: number;
  @ApiProperty({ type: "string", example: "kilos" })
  unidadMedida: string;
}
