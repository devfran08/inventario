import { Test, TestingModule } from '@nestjs/testing';
import { HerramientaController } from './herramienta.controller';
import { HerramientaService } from './herramienta.service';

describe('HerramientaController', () => {
  let controller: HerramientaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HerramientaController],
      providers: [HerramientaService],
    }).compile();

    controller = module.get<HerramientaController>(HerramientaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
