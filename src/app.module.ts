import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HerramientaModule } from './herramienta/herramienta.module';

@Module({
  imports: [HerramientaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
